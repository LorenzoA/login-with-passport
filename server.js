const express = require('express')
const app = express()

app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: false }))

const bcrypt = require('bcrypt')
const { name } = require('ejs')
const users = []

app.get('/', (req, res) => {

    res.render('index.ejs', { user: "name" })
})
// Login
app.get('/login', (req, res) => {

    res.render('login.ejs')
})
app.post('/login', (req, res) => {

})
// register
app.get('/register', (req, res) => {

    res.render('register.ejs')
})
app.post('/register', async (req, res) => {

    try {
        const hashedPwd = await bcrypt.hash(req.body.password, 10)
        users.push({
            nameid: Date.now().toString(),
            name: req.body.name,
            email: req.body.email,
            password: hashedPwd
        })
        res.redirect("/login")
    } catch {
        res.redirect("/register")
    }
    console.log(users);
})


app.listen(3000)